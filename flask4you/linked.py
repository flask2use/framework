from flask2use.graphs import *

################################################################################

from surf import *

store = Store(  reader='rdflib',
            writer='rdflib',
            rdflib_store = 'IOMemory')

session = Session(store)

print 'Load RDF data'
store.load_triples(source='http://www.w3.org/People/Berners-Lee/card.rdf')

Person = session.get_class(ns.FOAF['Person'])

all_persons = Person.all()

print 'Found %d persons that Tim Berners-Lee knows'%(len(all_persons))
for person in all_persons:
    print person.foaf_name.first

#create a person object
somebody = Person()
somebody_else = Person()

somebody.foaf_knows = somebody_else

################################################################################

class MyPerson(object):
        """ Some custom logic for foaf:Person resources. """

        def get_friends_count(self):
                return len(self.foaf_knows)


session.mapping[surf.ns.FOAF.Person] = MyPerson

# Now let's test the mapping
john = session.get_resource("http://example/john", surf.ns.FOAF.Person)

# Is `john` an instance of surf.Resource?
print isinstance(john, surf.Resource)
# outputs: True

# Is `john` an instance of MyPerson?
print isinstance(john, MyPerson)
# outputs: True

# Try the custom `get_friends_count` method:
print john.get_friends_count()
# outputs: 0

"""This file sets up a command line manager.

Use "python manage.py" for a list of available commands.
Use "python manage.py runserver" to start the development web server on localhost:5000.
Use "python manage.py runserver --help" for additional runserver options.
"""

from flask_migrate import MigrateCommand
from flask_script import Manager, Shell, Server

from app import Reactor, commands as CMD

# Setup Flask-Script with command line commands
manager = Manager(Reactor.initialize, with_default_commands=False)

manager.add_command('shell', CMD.shell.Interpret(use_ipython=False, make_context=CMD.shell.Contextualize))
manager.add_command('jupyt', CMD.shell.Interpret(use_ipython=True, make_context=CMD.shell.Contextualize))

manager.add_command('syncdb', CMD.trans.SyncDB)
manager.add_command('migrate', MigrateCommand)

manager.add_command('server', Server())

if __name__ == "__main__":
    # python manage.py                      # shows available commands
    # python manage.py runserver --help     # shows available runserver options
    manager.run()


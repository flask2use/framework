#!/usr/bin/env python3

import os,sys,click
from decouple import config as decoupled
import json,yaml

#*******************************************************************************

class ShellEx(object):
    def shell(self, *command):
        args = lambda value: ('"%s"' % value) if (" " in value) else value

        stmt = ' '.join([command] + [args(x) for x in argument])

        os.system(stmt)

    def exist(self,*x): return os.path.exists(self.rpath(*x))
    def folds(self,*x): return os.listdir(self.rpath(*x))

    def mkdir(self,*x): return os.system("mkdir -p %s" % self.rpath(*x))
    def touch(self,*x): return os.system("touch %s" % self.rpath(*x))

    rpath = lambda self,*x: os.path.join(self.bpath,*x)

#*******************************************************************************

class Artefact(object):
    flavor = property(lambda self: type(check).__name__.lower())

################################################################################

class Environ(Artefact):
    def __init__(self,parent,target,format=str):
        self.parent = parent
        self.target = target
        self.format = format

        if self.target in self.parent.data:
            self.value = self.parent.data[self.target]

    def __call__(self):
        if hasattr(self,'value'):
            if type(self.format) in (str,unicode):
                self.format = self.format.lower()

            if self.format in ['url']:
                try:
                    target = urlparse(self.value)
                except:
                    return False

            return True

        return False

    alias = property(lambda self: self.target)

#*******************************************************************************

class Folder(Artefact):
    def __init__(self,parent,target):
        self.parent = parent
        self.target = target

    def __call__(self):
        return self.parent.exist(self.target)

#*******************************************************************************

class Schema(Artefact):
    def __init__(self,parent,target,engine):
        self.parent = parent
        self.target = target
        self.engine = engine

    def __call__(self):
        return self.parent.exist(self.target)

#*******************************************************************************

class Address(object):
    def __init__(self,parent,target,engine,schema=None):
        self.parent = parent
        self.target = target
        self.engine = engine
        self.schema = schema

    def __call__(self):
        return self.target.exist(self.target)

################################################################################

class Project(ShellEx):
    def __init__(self,mgmt,kind,slug,**opts):
        self._mgmt = mgmt
        self._kind = kind
        self._slug = slug

        self._conf = opts
        self._data = {}

        if self.exist('.env'):
            for line in open(self.rpath('.env')).readlines():
                alias,value = line.split('=',1)

                self._data[alias] = value

        self.mgmt._item[self.name] = self

    mgmt = property(lambda self: self._mgmt)

    kind = property(lambda self: self._kind)
    slug = property(lambda self: self._slug)

    conf = property(lambda self: self._conf)
    data = property(lambda self: self._data)

    name = property(lambda self: self._conf.get('name',self.slug.capitalize()))

    def checks(self):
        for x in ('REDIS','DATABASE'):
            yield Environ(self, x+'_URL')

        for key in ('asset','pages'):
            if not self.exist(key):
                yield Folder(self,key)

        #for key in ('asset','pages'):
        #for key in ('theme','model','daten','graph','nerve','brain'):

    def __getitem__(self,key,default=None):
        if key in ('mgmt','kind','slug','name','conf','data','bpath'):
            return getattr(self,key,None)

        if key in self._data:
            return self._data[key]

        return default

    def __setitem__(self,alias,value):
        self._data[alias] = value

    def __dict__(self):
        resp = {}

        for meta in [self._data]:
            resp.update(meta)

        for name in ('mgmt','kind','slug','name','conf','data','bpath'):
            resp[name] = getattr(self,name,None)

        return resp

    bpath = property(lambda self: self.mgmt.rpath(self.kind,self.name))

#*******************************************************************************

class Manager(ShellEx):
    def __init__(self):
        self._item = {}
        self._plug = {}
        self._data = {}

        for pth in [
            '/git',
            "/media/%(USER)s/OffTheDock" % os.environ,
            os.path.dirname(__file__),
        ]:
            if os.path.exists(pth):
                self._root = pth

        self._test = decoupled('DEBUG',cast=bool,default=False)

    bpath = property(lambda self: self._root)
    def rpath(self,*x): return os.path.join(self.bpath, *x)
    debug = property(lambda self: self._test)

    project = property(lambda self: self._item)
    effects = property(lambda self: self._plug)

    def __getitem__(self,key,default=None):
        if key in self._item:
            return self._item[key]

        if key in self._data:
            return self._data[key]

        return default

    def __setitem__(self,alias,value):
        self._data[alias] = value

    def __call__(self,alias):
        if alias in self._item:
            check = []

            for entry in self[alias].checks():
                check.append(entry)

            for handle in self._plug:
                for entry in handle(self[alias]):
                    check.append(entry)

            for entry in check:
                if not entry():
                    yield entry

    def __dict__(self):
        resp = {}

        for meta in (self._data, self._item):
            resp.update(meta)

        #for name in ('bpath','rpath'):
        #    resp[name] = getattr(self,name,None)

        return resp

    def update(self, *args, **kwargs):
        for item in args:
            self._data.update(item)

        return self

    def extend(self, *args, **kwargs):
        def do_apply(handler, alias, **options):
            entry = handler

            if alias not in self._plug:
                self._plug[alias] = entry

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

################################################################################

mgr = Manager()

#*******************************************************************************

@mgr.extend('graph')
def is_graphql(item):
    yield Schema(self, 'graph/crawl.graphql','graphql')

    yield Schema(self, 'graph/proxy.graphql','graphql')

@mgr.extend('model')
def parse_server(item):
    yield Schema(self, 'graph/model.graphql','graphql')

    yield Schema(self, 'specs/model.js','nodejs')

@mgr.extend('daten')
def postgresql(item):
    #for x in ('KEY','EXT'):
    #    yield Environ(self, 'DATABASE_'+x)

    yield Schema(self, 'graph/daten.graphql','graphql')

#*******************************************************************************

@mgr.extend('hubot')
def is_hubot(item):
    for x in ('behavior','converse','dialects','extender'):
        yield Folder(self, 'hubot/'+x)

    yield Schema(self, 'graph/hubot.graphql','graphql')

@mgr.extend('index')
def search_kit(item):
    for x in ('LINK','NAME','SOCK','HITS'):
        yield Environ(self, 'ELASTIC_'+x)

    yield Schema(self, 'graph/index.graphql','graphql')

@mgr.extend('neo4j')
def neo4j_graphql(item):
    yield Schema(self, 'graph/neo4j.graphql','graphql')

    for x in ('LINK','USER','PASS'):
        yield Environ(self, 'NEO4J_'+x)

    yield Schema(self, 'query/neo4j.cypher','cypher')

#*******************************************************************************

@mgr.extend('react')
def react_native(item):
    obj['flag'] = []

    obj['unit'] = alias

    if os.path.exists("/git/reacts/%(unit)s/%(slug)s" % obj):
        obj['flag'] += ['react']

################################################################################

@click.group()
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx,debug):
    click.echo('Base dir : %(bpath)s' % ctx.obj)

    ctx.obj.update({
        'alias': [],
        'specs': {},
    })

    for alias in ('perso','organ','brand','agent'):
        lst,pth = [], ctx.obj.rpath('conf','yml','%s.yaml' % alias)

        for obj in yaml.load(open(pth)) : #, Loader=yaml.FullLoader):
            prj = Project(mgr,alias,**obj)

            lst.append(prj)

        ctx.obj['specs'][alias] = lst

        ctx.obj['alias'].append(alias)

    ctx.obj['debug'] = debug

    #click.echo(f"Debug mode is {'on' if debug else 'off'}")

################################################################################

@cli.command('setup')
@click.pass_context
def install(ctx):
    click.echo('Install :')

#*******************************************************************************

@cli.command('check')
@click.argument('alias', nargs=-1)
@click.pass_context
def validate(ctx,alias):
    click.echo('Validate :')

    alias = [x for x in alias]

    for entry in [y for x in ctx.obj['specs'].values() for y in x]:
        if len(alias)==0 or (entry.slug in alias):
            click.echo('\t*) %(slug)s : %(name)s' % entry)

            for check in entry.checks():
                click.echo('\t\t-> name: %s' % check.flavor)
                click.echo('\t\t   data: %s' % check.target)

                #if 'parse' in flag: click.echo('\t\t   <[Parse Server]>')
                #if 'react' in flag: click.echo('\t\t   <[React Native]>')

################################################################################

@cli.command('units')
@click.pass_context
def listing(ctx):
    click.echo('Listing :')

    for alias,specs in ctx.obj['specs'].items():
        click.echo('\t*) %s :' % alias)

        for entry in specs:
            click.echo('\t\t-> slug: %(slug)s' % entry)
            click.echo('\t\t   name: %(name)s' % entry)

            entry['flag'] = []

            if 'parse' in entry['flag']:
                click.echo('\t\t   <[Parse Server]>')

            if 'react' in entry['flag']:
                click.echo('\t\t   <[React Native]>')

            #click.echo('\t\t   unit: %s' % [x for x in entry.checks()])

################################################################################

@cli.command('parse')
@click.option('--run/--dry', default=True)
@click.option('--log/--quiet', default=True)
@click.pass_context
def parse_manager(ctx,run,log):
    key = {
        'client': "4f4613dc-0623-4543-8f91-7bf15c01630c",
        'master': "7c1ba58d-7a92-4ed5-9254-a97d09983234",
        'rest_x': "b20de6f2-9dc4-4ae8-82d3-714f7c17074b",
        'secret': "6e49d063-0b8d-4d0a-9a62-454f25428bf9",
    }

    lst = []

    click.echo('Generating Parse-Server configuration ...')

    for alias,specs in ctx.obj['specs'].items():
        click.echo('\t*) %s :' % alias)

        for entry in specs:
            entry['unit'] = alias

            click.echo('\t\t-> %(slug)s : %(name)s' % entry)

            entry['link'] = 'https://%(slug)s.herokuapp.com' % entry

            item = {
                "serverURL": "%(link)s/api/parse" % entry,
                "graphQLServerURL": "%(link)s/graph/obj"% entry,
                "supportedPushLocales": ["en", "fr"],
                "appId": entry['slug'],
                "masterKey": key['master'],
                "appName": "[%(unit)s] %(name)s" % entry,
            }

            item['iconName'] = "%(unit)s/%(slug)s.png" % entry

            lst.append(item)

    with open('%(HOME)s/parse.json' % os.environ,"w+") as f:
        f.write(json.dumps({
            "apps": lst,
            "iconsFolder": "images-directory/",
        }))

    click.echo('Executing Parse-Server via Docker ...')

    if run:
        key = 'parse-gui'

        shell("docker","stop",key)

        shell("docker","rm",key)

        shell("docker","run",
            "-v","%(HOME)s/parse.json:/src/Parse-Dashboard/parse-dashboard-config.json" % os.environ,
            "-v","%(HOME)s/Images/Logos/:/src/Parse-Dashboard/images-directory/" % os.environ,
            "-p","4040:4040","--name",key,"-d","parseplatform/parse-dashboard",
        "--dev")

    if log:
        shell("docker","logs","-f",key)

#*******************************************************************************

@cli.command('react')
@click.option('--expo/--no-expo', default=False)
@click.option('--yarn/--no-yarn', default=True)
@click.option('--save/--no-save', default=False)
@click.option('--push/--no-push', default=False)
@click.option('--all/--dry', default=False)
@click.pass_context
def react_native(ctx,expo,yarn,save,push,all):
    click.echo('#) Started React-Native operations :')

    for alias,specs in ctx.obj['specs'].items():
        click.echo('\t*) %s :' % alias)

        for entry in specs:
            entry['unit'] = alias

            click.echo('\t\t-> %(slug)s : %(name)s' % entry)

            entry['path'] = os.path.join('/git/reacts',entry['unit'],entry['slug'])

            if not os.path.exists(entry['path']):
                shell("git","clone","git@bitbucket.org:IT-React/%(slug)s.git" % entry,entry['path'])

            if not os.path.exists(entry['path']):
                shell('mkdir','-p',entry['path'])

            os.chdir(entry['path'])

            if (all or expo) and not os.path.exists('App.js'):
                    shell("expo","init",".","-t","blank")

            if (all or yarn) and os.path.exists('package.json'):
                shell("yarn","install")

            if (all or save or push):
                if not os.path.exists('.git'):
                    shell("git","init")

                    shell("git","remote","add","origin","git@bitbucket.org:IT-React/%(slug)s.git" % entry)

                if (all or save):
                    shell("git","add","--all")

                    shell("git","commit","-a","-m","'Summary of : %s'" % datetime.now())

                if (all or push):
                    shell("git","push","-u","origin","master")

            #entry['link'] = 'https://%(slug)s.herokuapp.com' % entry

    click.echo('#) Finished React-Native operations.')

################################################################################

if __name__ == '__main__':
    cli(
        obj=mgr,
    )

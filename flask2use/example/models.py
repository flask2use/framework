from flask2use.models import *

################################################################################

@core.model('cloud')
class PlatformCloud(core.Model):
    id = core.field(int, primary_key=True)
    alias = core.field(str,64, unique=True, nullable=False)
    title = core.field(str,255)

    oauth = core.field(bool)
    robot = core.field(bool)
    token = core.field(bool)

    begin = core.field(time)

    def __str__(self):
        return self.email

#*******************************************************************************

@core.model('oauth')
class PlatformOAuth(core.Model):
    id = core.field(int, primary_key=True)
    cloud = core.field(str,64, unique=True, nullable=False)
    email = core.field(str,255, unique=True, nullable=False)
    token = core.field(str,255, nullable=False)

    verif = core.field(bool)
    begin = core.field(time)

    def __str__(self):
        return self.email

################################################################################

@core.model('robot')
class PlatformRobot(core.Model):
    id = core.field(int, primary_key=True)
    cloud = core.field(str,64, unique=True, nullable=False)
    email = core.field(str,255, unique=True, nullable=False)
    token = core.field(str,255, nullable=False)

    verif = core.field(bool)
    begin = core.field(time)

    def __str__(self):
        return self.email

#*******************************************************************************

@core.model('token')
class PlatformToken(core.Model):
    id = core.field(int, primary_key=True)
    cloud = core.field(str,64, unique=True, nullable=False)
    email = core.field(str,255, unique=True, nullable=False)
    token = core.field(str,255, nullable=False)

    verif = core.field(bool)
    begin = core.field(time)

    def __str__(self):
        return self.email

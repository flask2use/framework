from app.shortcuts import 

#@Reactor.odm.schema(Post, content=ContentResource)
class PostResource(Reactor.odm.Resource):
    filters = {
        'title': [ops.Exact, ops.Startswith],
        'author_id': [ops.Exact],
    }
    rename_fields = {
        'author': 'author_id',
    }

@Reactor.odm.publish('posts', PostResource, '/posts/')
class PostView(Reactor.odm.Viewer):
    methods = [methods.Create, methods.Update, methods.Fetch, methods.List]


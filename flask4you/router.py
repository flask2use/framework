from flask2use.panels import *

#*******************************************************************************

@core.route('/media/<path:filename>')
def media(filename):
    return send_from_directory(
        app.config['UPLOAD_FOLDER'],
        filename,
        as_attachment=True
    )

################################################################################

@core.route('/')
def index():
    return render_template('index.html')

#*******************************************************************************

@core.route('/submit', methods=['GET', 'POST'])
def submit():
    form = MyForm()
    if form.validate_on_submit():
        entry = Message("Hello",
                      sender="from@example.com",
                      recipients=["to@example.com"])

        core.msg.send(entry)

        return redirect('/success')

    return render_template('submit.html', form=form)

################################################################################

@core.route("/slow")
#@core.eta.limit("1 per day")
def slow():
    return ":("

#*******************************************************************************

@core.route("/medium")
#@core.eta.limit("1/second", override_defaults=False)
def medium():
    return ":|"

#*******************************************************************************

@core.route("/fast")
def fast():
    return ":)"

#*******************************************************************************

@core.route("/ping")
#@core.eta.exempt
def ping():
    return "PONG"

#*******************************************************************************

@core.route("/where")
def mapview():
    # creating a map in the view
    mymap = Map(
        identifier="view-side",
        lat=37.4419,
        lng=-122.1419,
        markers=[(37.4419, -122.1419)]
    )
    sndmap = Map(
        identifier="sndmap",
        lat=37.4419,
        lng=-122.1419,
        markers=[
          {
             'icon': 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
             'lat': 37.4419,
             'lng': -122.1419,
             'infobox': "<b>Hello World</b>"
          },
          {
             'icon': 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
             'lat': 37.4300,
             'lng': -122.1400,
             'infobox': "<b>Hello World from other place</b>"
          }
        ]
    )
    return render_template('whereis.html', mymap=mymap, sndmap=sndmap)

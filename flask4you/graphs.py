from flask2use.formal import *

from ariadne import gql, QueryType, make_executable_schema

from ariadne import gql, QueryType, make_executable_schema, graphql_sync

################################################################################

type_defs = gql(
   """
   type Query {
       places: [Place]
   }


   type Place {
       name: String!
       description: String!
       country: String!
       }

   type Mutation{add_place(name: String!, description: String!, country: String!): Place}
   """
)

#*******************************************************************************

query = QueryType()
mutation = MutationType()

################################################################################

@query.field("places")
def places(*_):
   return [
       {"name": "Paris", "description": "The city of lights", "country": "France"},
       {"name": "Rome", "description": "The city of pizza", "country": "Italy"},
       {
           "name": "London",
           "description": "The city of big buildings",
           "country": "United Kingdom",
       },
   ]

#*******************************************************************************

@mutation.field("add_place")
def add_place(_, info, name, description, country):
   places.append({"name": name, "description": description, "country": country})
   return {"name": name, "description": description, "country": country}

################################################################################

schema = make_executable_schema(type_defs, query)

#*******************************************************************************

core.app.add_url_rule('/graphql', view_func=GraphQLView.as_view(
    'graphql',
    schema=schema,
    graphiql=True,
))

#*******************************************************************************

core.app.add_url_rule('/graphql/batch', view_func=GraphQLView.as_view(
    'graphql',
    schema=schema,
    batch=True
))

# Optional, for adding batch query support (used in Apollo-Client)

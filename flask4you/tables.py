from flask2use.config import *

################################################################################

core.table(
    'roles_users',
    core.orm.Column('role_id', core.orm.Integer(), core.orm.ForeignKey('role.id')),
    core.orm.Column('user_id', core.orm.Integer(), core.orm.ForeignKey('user.id')),
)

#*******************************************************************************

core.table(
    'teams_users',
    core.orm.Column('team_id', core.orm.Integer(), core.orm.ForeignKey('team.id')),
    core.orm.Column('user_id', core.orm.Integer(), core.orm.ForeignKey('user.id')),
)

#*******************************************************************************

from flask2use.engine import *

"""
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.1/socket.io.js" integrity="sha512-q/dWJ3kcmjBLU4Qc47E4A9kTB4m3wuTY7vkFJDTZKjTs8jhyGQnaUrxa0Ytd0ssMZhbNua9hE+E7Qv1j+DyZwA==" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf-8">
var socket = io();
socket.on('connect', function() {
socket.emit('my event', {data: 'I\'m connected!'});
});
</script>
"""

################################################################################

@core.cnx.on_error()        # Handles the default namespace
def error_handler(e):
    pass

@core.cnx.on_error('/chat') # handles the '/chat' namespace
def error_handler_chat(e):
    pass

@core.cnx.on_error_default  # handles all namespaces without an explicit error handler
def default_error_handler(e):
    pass

################################################################################

@core.cnx.on('connect')
def test_connect(auth):
    emit('my response', {'data': 'Connected'})

@core.cnx.on('disconnect')
def test_disconnect():
    print('Client disconnected')

################################################################################

@core.cnx.on('join')
def on_join(data):
    username = data['username']
    room = data['room']
    join_room(room)
    send(username + ' has entered the room.', to=room)

#*******************************************************************************

@core.cnx.on('leave')
def on_leave(data):
    username = data['username']
    room = data['room']
    leave_room(room)
    send(username + ' has left the room.', to=room)

################################################################################

@core.cnx.on('message')
def handle_message(message):
    send(message, namespace='/chat')

@core.cnx.on('customs')
def handle_my_custom_event(json):
    emit('my response', ('foo', 'bar', json), namespace='/chat')

@core.cnx.on('json')
def handle_json(json):
    print('received json: ' + str(json))

#*******************************************************************************

@core.cnx.on('borader')
def handle_my_custom_event(data):
    emit('my response', data, broadcast=True)

#*******************************************************************************

@core.cnx.event
def my_custom_event(arg1, arg2, arg3):
    print('received args: ' + arg1 + arg2 + arg3)

#*******************************************************************************

def ack():
    print('message was received!')

@core.cnx.on('mine')
def handle_my_custom_event(json):
    emit('my response', json, callback=ack)

#*******************************************************************************

@core.cnx.on('your', namespace='/test')
def handle_my_custom_namespace_event(json):
    print('received json: ' + str(json))

#*******************************************************************************

def my_function_handler(data):
    pass

core.cnx.on_event('their', my_function_handler, namespace='/test')

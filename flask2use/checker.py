#!/usr/bin/env python3

import os,sys,re,json

spath = os.path.dirname(__file__)
spath = "/git/unitar"
rpath = lambda *x: os.path.join(spath,*x)
spath = "/media/%(USER)s/Coding/unitar" % os.environ

if False and not os.path.exists(spath):
    for pth in [
        "/media/%(USER)s/Coding/unitar" % os.environ,
        "/media/%(USER)s/OffTheDock/unit" % os.environ,
        os.path.dirname(__file__),
    ]:
        if os.path.exists(pth):
            spath = pth

################################################################################

class Folder(object):
    def __init__(self,parent,target,folder):
        self.parent = parent
        self.target = target
        self.folder = folder

    def __call__(self):
        return self.parent.exist(self.folder)

#*******************************************************************************

class Schema(object):
    def __init__(self,parent,target,engine):
        self.parent = parent
        self.target = target
        self.engine = engine

    def __call__(self):
        return self.parent.exist(self.target)

#*******************************************************************************

class Endpoint(object):
    def __init__(self,parent,target,engine,schema=None):
        self.parent = parent
        self.target = target
        self.engine = engine
        self.schema = schema

    def __call__(self):
        return self.target.exist(self.target)

################################################################################

class ShellEx(object):
    def shell(self, *command):
        stmt = " ".join(command)

        os.system(stmt)

    def exist(self,*x): return os.path.exists(self.rpath(*x))
    def folds(self,*x): return os.listdir(self.rpath(*x))

    def mkdir(self,*x): return os.system("mkdir -p %s" % self.rpath(*x))
    def touch(self,*x): return os.system("touch %s" % self.rpath(*x))

    rpath = lambda *x: os.path.join(self.bpath,*x)

#*******************************************************************************

class Project(ShellEx):
    def __init__(self,prnt,kind,name):
        self._mgmt = mgmt
        self._kind = kind
        self._name = name

    mgmt = property(lambda self: self._mgmt)
    name = property(lambda self: self._name)
    kind = property(lambda self: self._kind)

    bpath = property(lambda self: os.path.join(self.spath,self.kind,self.name))

    def checks(self):
        for key in ('asset','pages'): #,'theme','model','daten','graph','nerve','brain'):
            if not self.exist(key):
                yield Folder(self,key)

        #for key in ('asset','pages'):
        #for key in ('asset','pages','theme','model','daten','graph','nerve','brain'):

#*******************************************************************************

class Manager(ShellEx):
    def __init__(self):
        self._item = {}
        self._plug = {}

    project = property(lambda self: self._item)
    effects = property(lambda self: self._plug)

    def __getitem__(self,key,default=None):
        return self._item.get(key,default)

    def checks(self,*keys):
        check = []

        for alias in self._item.keys():
            for entry in self._item[alias].checks():
                check.append(entry)

        for alias in keys:
            for handl in self._plug[alias]:
                for entry in handl(self[alias]):
                    check.append(entry)

        for entry in check:
            if not entry():
                yield entry

    def extend(self, *args, **kwargs):
        def do_apply(handler, alias, **options):
            entry = handler

            if alias not in self._item:
                self._plug[alias] = []

            if entry not in self._plug[alias]:
                self._plug[alias].append(entry)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

mgr = Manager()

################################################################################

@mgr.extend('hubot')
def is_hubot(item):
    yield Folder(self, 'hubot/')

#*******************************************************************************

@mgr.extend('parse')
def is_parse(item):
    yield Schema(self, 'graph/parse.graphql','graphql')

    yield Schema(self, 'engine/parse.js','nodejs')

#*******************************************************************************

@mgr.extend('parse')
def is_neo4j(item):
    yield Schema(self, 'graph/neo4j.graphql','graphql')

    yield Schema(self, 'daten/neo4j.cypher','cypher')

################################################################################

def shell(prog,*args):
    stmt = ' '.join([prog]+args)

    print("SHELL> %s" % stmt)

    os.system(stmt)

################################################################################

def packages(kind,name,flag):
    spec = {
        "autobahn2use": "0.0.6",
        "dataset2use": "0.1.1",
        "express2use": "0.1.4",
        "filesystem2use": "0.0.6",
        "graphql2use": "^0.1.2",
        "hubot2use": "0.3.1",
        "hubot4you": "0.0.1",
        "linguist2use": "0.1.1",
        "mosquitto2use": "0.1.1",
        "network2use": "0.0.3",
        "vocabular2use": "0.0.7",
    }
    path = rpath(kind,name,"package.json")

    if not os.path.exists(path):
        print("[%s](%s) Manifest not found" % (kind,name))

        if not flag:
            return

    try:
        info = open(json.loads(path).read())
    except Exception as ex:
        print("[%s](%s) Manifest not exact : %s" % (kind,name,ex))

        return

    for k,v in spec.items():
        if k in info['dependencies']:
            if v not in info['dependencies'][k]:
                print("[%s](%s) Conflict dependency '%s' at : %s != %s" % (kind,name,k,v,info['dependencies'][k]))

                if flag:
                    info['dependencies'][k] = v
        else:
            if flag:
                info['dependencies'][k] = v

            print("[%s](%s) Missing dependency '%s' at : %s" % (kind,name,k,v))

    if flag:
        with open(path,'w+') as f:
            f.write(json.dumps(info))

################################################################################

def itemloop(kind,name):
    for key in ('asset','pages','theme','model','daten','graph','nerve','brain'):
        pth = rpath(kind,name,key)

        if not os.path.exists(pth):
            shell("mkdir","-p","%s" % pth)

            shell("touch","%s/.gitkeep" % pth)

    if os.path.exists("/git/heroku/"+name):
        data = os.listdir(rpath("/git/heroku/"+name))

        if len(data)==1 and ('.git' in data):
            print("Confusing Heroku app : %s" % name)

            shell("rm","-fR","/git/heroku/%s" % name)

    packages(kind,name,False)

def mainloop():
    for nrw in ('agent','brand','memex','organ','perso','space'):
        for key in os.listdir(rpath(nrw)):
            itemloop(nrw,key)

################################################################################

if __name__=="__main__":
    mainloop()

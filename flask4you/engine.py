from flask2use.formal import *
#from flask2use.linked import *

################################################################################

@core.rpc.method('App.index')
def index() -> str:
    return 'Welcome to Flask JSON-RPC'

################################################################################

@core.xpc.register
def hello(name="world"):
    if not name:
        raise Fault("unknown_recipient", "I need someone to greet!")
    return "Hello, %s!" % name

#*******************************************************************************

blog = core.xpc.namespace('blog')

@blog.register
def add_post(title, text):
    # do whatever...
    pass

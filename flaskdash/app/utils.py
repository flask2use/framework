from app.helpers import *

class ObjectMapper(Dispatcher):
    def __init__(self, *args, **kwargs):
        self._std = {}
        self._reg = {}

        self.trigger('bootstrap', *args, **kwargs)

    def declare(self, *args, **kwargs):
        def do_apply(handler, *listing):
            for alias in listing:
                if alias not in self._std:
                    self._std[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def register(self, *args, **kwargs):
        return lambda hnd: self.registering(hnd, *args, **kwargs)

    def publish(self, *args, **kwargs):
        return lambda hnd: self.publishing(hnd, *args, **kwargs)

#************************************************************************************************

class ORM(ObjectMapper):
    def bootstrap(self, *args, **kwargs):
        self.sql = SQLAlchemy()
        self.mgr = Migrate()

        for key,obj in dict(
            Model = self.mgr.Model,
        ).iteritems():
            setattr(self, key, obj)

    def initialize(self, app):
        # Setup Flask-SQLAlchemy
        self.sql.init_app(app)
        # Setup Flask-Migrate
        self.mgr.init_app(app, self.sql)

    #**********************************************************************************************

    def registering(self, handler, alias, **options):
        self.mgmt.register(User, session=self.sql.session)

        return handler

#************************************************************************************************

class ODM(ObjectMapper):
    def bootstrap(self, *args, **kwargs):
        self.mgr = MongoEngine()
        self.api = MongoRest()

        for key,obj in dict(
            Model = self.mgr.Document,
            Embed = self.mgr.EmbeddedDocument,

            Schema = self.api.Resource,
            Viewer = self.mgr.ResourceView,
        ).iteritems():
            setattr(self, key, obj)

    def initialize(self, app):
        # Setup MongoEngine + REST
        self.mgr.init_app(app)
        self.api.init_app(app)

    #**********************************************************************************************

    def registering(self, handler, alias, **options):
        self.mgmt.register(User, session=self.sql.session)

        return handler

    def schematize(self, handler, document, **related):
        setattr(handler, 'document', document)
        setattr(handler, 'related_resources', related)

        return handler

    def publishing(self, handler, name, schema, link):
        setattr(handler, 'resource', schema)

        self.api.register(name=name, url=link)(handler)

        return handler

#************************************************************************************************

class OGM(ObjectMapper):
    def bootstrap(self, *args, **kwargs):
        pass

    def initialize(self, app):
        pass

    #**********************************************************************************************

    def registering(self, handler, alias, **options):
        #self.mgmt.register(User, session=self.sql.session)

        return handler

#################################################################################################

@singleton
class Reactor(Dispatcher):
    def __init__(self, *args, **kwargs):
        self.trigger('bootstrap', *args, **kwargs)

    def bootstrap(self, *args, **kwargs):
        self.csrf = CSRFProtect()
        self.mail = Mail()

        for hnd in (ORM,ODM,OGM):
            key = hnd.__name__.lower()

            setattr(self, 

    def initialize(self, extra_config_settings={}):
        """Create a Flask application.
        """
        # Instantiate Flask
        app = Flask(__name__)

        # Load App Config settings
        app.config.from_object(rq_dashboard.default_settings)
        # Load common settings from 'app/settings.py' file
        app.config.from_object('app.settings')
        # Load local settings from 'app/local_settings.py'
        app.config.from_object('app.local_settings')
        # Load extra config settings from 'extra_config_settings' param
        app.config.update(extra_config_settings)

        # Setup Flask-Mail
        self.mail.init_app(app)

        # Setup WTForms CSRFProtect
        self.csrf.init_app(app)

        # Setup Flask-User to handle user account related forms
        from .models.identity import User, MyRegisterForm
        from .views.misc_views import user_profile_page

        self.user = UserManager(app, self.sql, User)

        from flask.ext.superadmin import Admin, model

        self.mgmt = Admin(app)

        self.register_model()(User)

        from flask.ext.superadmin.contrib import fileadmin

        # Adding a custom view
        #self.mgmt.add_view(CustomView(name='Photos', category='Cats'))

        self.mgmt.add_view(fileadmin.FileAdmin(path, '/files/', name='Files'))

        self.mgmt.setup_app(app)

        # Register blueprints
        from app.views.misc_views import main_blueprint
        from app.views.apis import api_blueprint
        app.register_blueprint(main_blueprint)
        app.register_blueprint(api_blueprint)
        csrf_protect.exempt(api_blueprint)
        
        # Register blueprints
        from app.views.misc_views import main_blueprint
        app.register_blueprint(main_blueprint)

        app.register_blueprint(rq_dashboard.blueprint, url_prefix="/queues")

        # Define bootstrap_is_hidden_field for flask-bootstrap's bootstrap_wtf.html
        from wtforms.fields import HiddenField

        def is_hidden_field_filter(field):
            return isinstance(field, HiddenField)

        app.jinja_env.globals['bootstrap_is_hidden_field'] = is_hidden_field_filter

        # Setup an error-logger to send emails to app.config.ADMINS
        self.email_error(app)

        self.middlewares(app)

        return app

    #**********************************************************************************************

    def email_error(self, app):
        """
        Initialize a logger to send emails on error-level messages.
        Unhandled exceptions will now send an email message to app.config.ADMINS.
        """
        if app.debug: return  # Do not send error emails while developing

        # Retrieve email settings from app.config
        host = app.config['MAIL_SERVER']
        port = app.config['MAIL_PORT']
        from_addr = app.config['MAIL_DEFAULT_SENDER']
        username = app.config['MAIL_USERNAME']
        password = app.config['MAIL_PASSWORD']
        secure = () if app.config.get('MAIL_USE_TLS') else None

        # Retrieve app settings from app.config
        to_addr_list = app.config['ADMINS']
        subject = app.config.get('APP_SYSTEM_ERROR_SUBJECT_LINE', 'System Error')

        # Setup an SMTP mail handler for error-level messages
        import logging
        from logging.handlers import SMTPHandler

        mail_handler = SMTPHandler(
            mailhost=(host, port),  # Mail host and port
            fromaddr=from_addr,  # From address
            toaddrs=to_addr_list,  # To address
            subject=subject,  # Subject line
            credentials=(username, password),  # Credentials
            secure=secure,
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

        # Log errors using: app.logger.error('Some error message')

    #**********************************************************************************************

    def middlewares(self, app):
        from flask_gzip import Gzip

        gzip = Gzip(app)

        from flask.ext.bcrypt import Bcrypt

        self.crypt = Bcrypt(app)

        from flask_jsonrpc import JSONRPC

        self.jsrpc = JSONRPC(app, '/api')

        from flask_limiter import Limiter

        self.limiter = Limiter(
            app,
            key_func=get_remote_address,
            default_limits=["2 per minute", "1 per second"],
        )

        from flask.ext.rq import RQ

        self.queue = RQ(app)

        from flask_debugtoolbar import DebugToolbarExtension

        self.debug = DebugToolbarExtension(app)

        from flask_assets import Environment, Bundle

        self.asset = Environment(app)

        #js = Bundle('jquery.js', 'base.js', 'widgets.js', filters='jsmin', output='gen/packed.js')

        #assets.register('js_all', js)

        from flask_vue import Vue

        self.vuejs = Vue(app)

    #**********************************************************************************************

    def register_views(self, *args, **kwargs):
        def do_apply(handler, alias, **options):
            

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def register_admin(self, *args, **kwargs):
        def do_apply(handler, alias, **options):
            

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)


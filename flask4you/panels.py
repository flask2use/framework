from flask2use.queues import *

################################################################################

@core.admin(Role, menu_icon_type='fa', menu_icon_value='fa-server', name="Roles")
@core.admin(Team, menu_icon_type='fa', menu_icon_value='fa-building', name="Teams")
class DefaultView(core.Generic):
    pass

#*******************************************************************************

@core.admin(User, menu_icon_type='fa', menu_icon_value='fa-users', name="Users")
class UserView(core.Generic):
    column_editable_list = ['email', 'first_name', 'last_name']
    column_searchable_list = column_editable_list
    column_exclude_list = ['password']
    #form_excluded_columns = column_exclude_list
    column_details_exclude_list = column_exclude_list
    column_filters = column_editable_list
    form_overrides = {
        'password': PasswordField
    }

#*******************************************************************************

#@core.admin(PlatformCloud, menu_icon_type='fa', menu_icon_value='fa-cloud', name="Platform Cloud")
#@core.admin(PlatformOAuth, menu_icon_type='fa', menu_icon_value='fa-lock', name="Platform OAuth")
#@core.admin(PlatformToken, menu_icon_type='fa', menu_icon_value='fa-keys', name="Platform Token")
class PlatformView(core.Generic):
    pass

################################################################################

@core.panel(name="Custom view", endpoint='custom', menu_icon_type='fa', menu_icon_value='fa-connectdevelop')
class CustomView(core.Viewer):
    @expose('/')
    def index(self):
        return self.render('admin/custom_index.html')

# Create customized model view class

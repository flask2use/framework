
from setuptools import setup, find_packages

setup(
    name='flask2use',
    version='0.0.3',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='Pythonic extensions for the Flask framework',
    long_description=open('README.txt').read(),
    install_requires=[
        'rq-dashboard',
            'enum34==1.1.6',
            'email_validator',
        'Flask',
            'Flask-BCrypt',
            'Flask-CORS',
            'Flask-GZip',
            'Flask-JWT',
            'Flask-Limiter',
            'Flask-Mail',
            'Flask-RQ2',
            'Flask-WTF',
        'Flask-Admin',
            'Flask-DebugToolbar',
            'Flask-SQLAlchemy',
            'Flask-Security>=1.7.5',
        'Flask-GoogleMaps',
            'Flask-JsonRPC',
            'Flask-GraphQL',
            'Flask-XML-RPC',
        'Flask-File-Upload',
    ],
    url='https://github.com/flask2use/framework',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)


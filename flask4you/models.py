from flask2use.tables import *

################################################################################

@core.model()
class Role(core.Model, RoleMixin):
    id = core.field(int, primary_key=True)
    name = core.field(str, 80, unique=True)
    description = core.field(str, 255)

    def __str__(self):
        return self.name

#*******************************************************************************

@core.model()
class User(core.Model, UserMixin):
    id = core.field(int,primary_key=True)
    first_name = core.field(str,255)
    last_name = core.field(str,255)
    email = core.field(str,255, unique=True, nullable=False)
    password = core.field(str,255, nullable=False)
    active = core.field(bool)
    confirmed_at = core.field(time)

    roles = core.pivot('Role','roles_users','users', lazy='dynamic')

    # Custom User Payload
    def get_security_payload(self):
        return {
            'id': self.id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email
        }

    def __str__(self): return self.email

#*******************************************************************************

class Team(core.Model):
    id = core.field(int, primary_key=True)
    alias = core.field(str,255, nullable=False)
    title = core.field(str,255)
    email = core.field(str,255, unique=True, nullable=False)

    token = core.field(str,255, nullable=False)
    verif = core.field(bool)

    cover = core.ups.Column()
    image = core.ups.Column()
    begin = core.field(time)

    users = core.pivot('User','teams_users','teams', lazy='dynamic')

    def __str__(self):
        return self.title or self.alias

from app.shortcuts import *

# When using a Flask app factory we must use a blueprint to avoid needing 'app' for '@app.route'
api_blueprint = Blueprint('api', __name__, template_folder='templates')

@api_blueprint.route('/sample_call', methods=['POST'])
def sample_page():

    ret = {"sample return": 10}
    return(jsonify(ret), 200)

@api_blueprint.route('/slower')
@limiter.limit("1 per day")
def slow():
    return "24"

@api_blueprint.route('/faster')
def fast():
    return "42"


from app.shortcuts import 

class Person(Reactor.nosql.Document):
    email = db.EmailField(unique=True, required=True)

class Content(Reactor.nosql.EmbeddedDocument):
    text = db.StringField()

class ContentResource(Resource):
    document = Content

class Post(Reactor.nosql.Document):
    title = Reactor.nosql.StringField(max_length=120, required=True)
    author = Reactor.nosql.ReferenceField(Person)
    content = Reactor.nosql.EmbeddedDocumentField(Content)


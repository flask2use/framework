from flask import Flask
from flask_mongorest import MongoRest
from flask_mongorest.views import ResourceView
from flask_mongorest.resources import Resource
from flask_mongorest import operators as ops
from flask_mongorest import methods

# __init__.py is a special Python file that allows a directory to become
# a Python package so it can be accessed using the 'import' statement.

# __init__.py is a special Python file that allows a directory to become
# a Python package so it can be accessed using the 'import' statement.

from datetime import datetime
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mongoengine import MongoEngine
from flask_migrate import Migrate, MigrateCommand

from flask_wtf.csrf import CSRFProtect
from flask_mail import Mail

from flask_user import UserManager, UserMixin

class Dispatcher(object):
    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)

def singleton(handler):
    return lambda *args,**kwargs: handler(*args,**kwargs)

from flask_limiter.util import get_remote_address

from flask.ext.rq import job, get_queue, get_worker
import rq_dashboard


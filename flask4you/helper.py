import os,re,sys,click
from datetime import date,time,datetime,timezone
import string,random
from redis import Redis
from flask import Flask, url_for, redirect, render_template, request, abort, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from flask_security import SQLAlchemyUserDatastore, UserMixin, RoleMixin
from flask_security import Security, login_required, current_user
from flask_security.utils import encrypt_password
import flask_admin
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from flask_admin import BaseView, expose
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin.contrib.rediscli import RedisCli
from wtforms import PasswordField
from flask_cors import CORS, cross_origin
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_graphql import GraphQLView
from flask_debugtoolbar import DebugToolbarExtension
from flask_mail import Mail, Message
from flask_rq2 import RQ
from flask_gzip import Gzip
from flask_bcrypt import Bcrypt
import rq_dashboard
from flask_jsonrpc import JSONRPC
from flask_xmlrpcre.xmlrpcre import XMLRPCHandler, Fault
from flask_socketio import SocketIO, emit, send, join_room, leave_room
from flask_googlemaps import Map, GoogleMaps
from flask_file_upload.file_upload import FileUpload

################################################################################

class Reactor(object):
    def __init__(self,*args,**opts):
        self.app = Flask('diflood')

        self.app.config.from_pyfile('config.py')

        self._key = {
            'bool':  bool,
            'int':   int,
            'str':   str,
            'float': float,
            'date':  date,
            'time':  time,
        }
        self.fix = []
        self.sch = {}
        self.rel = {}

        self.orm = SQLAlchemy(self.app)

        self._key.update({
            bool:  self.orm.Boolean,
            int:   self.orm.Integer,
            str:   self.orm.String,
            #float: self.orm.String,
            date:   self.orm.Date,
            time:   self.orm.DateTime,
        })

        setattr(self, 'Model', self.orm.Model)

    def before(self):
        self.idp = SQLAlchemyUserDatastore(self.orm, self.sch['user'], self.sch['role'])
        self.sec = Security(self.app, self.idp, register_blueprint=True)

        Bcrypt(self.app)

        @self.sec.context_processor
        def security_context_processor():
            return dict(
                admin_base_template=self.mgr.base_template,
                admin_view=self.mgr.index_view,
                h=admin_helpers,
                get_url=url_for
            )

        CORS(self.app, resources={r"/api/*": {"origins": "*"}})

        self.msg = Mail(self.app)
        self.que = RQ(self.app)

        Gzip(self.app)

        self.rpc = JSONRPC(self.app, '/rpc/json', enable_web_browsable_api=True)

        self.xpc = XMLRPCHandler('api')
        self.xpc.connect(self.app, '/rpc/xml')

        GoogleMaps(self.app)

        self.cnx = SocketIO(self.app)

        self.ups = FileUpload(self.app, self.orm)

    def finish(self):
        self.mgr = flask_admin.Admin(
            self.app,
            'My Dashboard',
            base_template='my_master.html',
            template_mode='bootstrap4',
        )
        #self.eta = Limiter(
        #    self.app,
        #    key_func=get_remote_address,
        #    default_limits=["3000 per minute", "1 per second"],
        #)
        self.bar = DebugToolbarExtension(self.app)

        self.app.config.from_object(rq_dashboard.default_settings)
        self.app.register_blueprint(rq_dashboard.blueprint, url_prefix="/que")

        self.mgr.add_view(RedisCli(Redis()))

        #self.mgr.add_view(FileAdmin("/media/chromebook/Coding/heroku/diflood/front/diflood/static", '/static/', name='Asset Directory'))

        self.mgr.add_view(FileAdmin(self.app.config['UPLOAD_FOLDER'], '/media/', name='Media Directory'))

    def syncdb(self, reset=False, loads=False):
        # Build a sample db on the fly, if one does not exist yet.
        app_dir = os.path.realpath(os.path.dirname(__file__))

        database_path = os.path.join(app_dir, self.app.config['DATABASE_FILE'])

        with self.app.app_context():
            if reset or not os.path.exists(database_path):
                self.orm.drop_all()
                self.orm.create_all()

            if loads:
                for handler in self.fix:
                    handler()
        """
        Populate a small db with some example entries.
        """
        return

    def fixture(self, *args, **kwargs):
        def do_apply(handler, **options):
            self.fix.append(handler)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def __call__(self,*args,**opts):
        pass

    def __getitem__(self,name):
        if name in self.rel:
            return self.rel[name]
        else:
            return None

    def model(self, *args, **kwargs):
        def do_apply(handler, alias=None, **options):
            if alias is None:
                alias = handler.__name__.lower()

            if alias not in self.sch:
                self.sch[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def table(self,alias,*args,**opts):
        if alias not in self.rel:
            self.rel[alias] = self.orm.Table(alias,*args,**opts)

        return self.rel.get(alias,None)

    def field(self,name,size=None,**opts):
        while name in self._key:
            name = self._key[name]

        if size is None:
            kind = name()
        else:
            kind = name(size)

        resp = self.orm.Column(kind,**opts)

        return resp

    def pivot(self,alias,field,table,*args,**opts):
        resp = None

        if field in self.rel:
            resp = self.rel[field]

            back = self.orm.backref(table, lazy='dynamic')

            resp = self.orm.relationship(alias, secondary=resp, backref=back)

        return resp

    def admin(self, *args, **kwargs):
        def do_apply(handler, schema, **options):
            self.mgr.add_view(handler(
                schema,
                self.orm.session,
            **options))

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def panel(self, *args, **kwargs):
        def do_apply(handler, *params, **options):
            self.mgr.add_view(handler(*params, **options))

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def route(self, *args, **kwargs): return self.app.route(*args, **kwargs)

    ############################################################################

    class Generic(sqla.ModelView):
        def is_accessible(self):
            if not current_user.is_active or not current_user.is_authenticated:
                return False

            if current_user.has_role('superuser'):
                return True

            return False

        def _handle_view(self, name, **kwargs):
            """
            Override builtin _handle_view in order to redirect users when a view is not accessible.
            """
            if not self.is_accessible():
                if current_user.is_authenticated:
                    # permission denied
                    abort(403)
                else:
                    # login
                    return redirect(url_for('security.login', next=request.url))

        # can_edit = True
        edit_modal = True
        create_modal = True
        can_export = True
        can_view_details = True
        details_modal = True

    Viewer = BaseView

    #***************************************************************************

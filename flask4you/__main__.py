from .config import *

################################################################################

@click.group()
@click.option('--debug/--no-debug', default=False)
@click.pass_context
def cli(ctx,debug):
    #click.echo(f"Debug mode is {'on' if debug else 'off'}")

    ctx.obj['debug'] = debug

#*******************************************************************************

@cli.command('local')
@click.pass_context
def synchroniz(ctx):
    click.echo('Created')

    core.syncdb(reset=True, loads=True)

################################################################################

@cli.command('serve')
@click.pass_context
def web_server(ctx):
    click.echo('Routing')

    # Start app
    core.app.run(debug=True)

#*******************************************************************************

@cli.command('queue')
@click.pass_context
def que_worker(ctx):
    instance = core.que.get_worker('low', 'simple')
    # Creates a worker that handle jobs in both ``simple`` and ``low`` queues.
    instance.work(burst=True)

#*******************************************************************************

@cli.command('clock')
@click.pass_context
def scheduler(ctx):
    instance = core.que.get_scheduler(interval=10)
    # check every 10 seconds if there are any jobs to enqueue
    instance.run()

################################################################################

@cli.command('maker')
@click.pass_context
def wizardery(ctx):
    click.echo('Created')

#*******************************************************************************

if __name__ == '__main__':
    cli(obj={

    })

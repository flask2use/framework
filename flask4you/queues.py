from flask2use.socket import *

################################################################################

@core.que.job
def add(x, y):
    return x + y

#*******************************************************************************

@core.que.job('low', timeout=60)
def add(x, y):
    return x + y

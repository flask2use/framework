from flask2use.helper import *

################################################################################

class Nucleon(Reactor):
    def engine(self, **kws):
        pass

    def socket(self, **kws):
        pass

    def router(self, **kws):
        pass

    def worker(self, **kws):
        pass

#*******************************************************************************

core = Nucleon()

################################################################################

@core.fixture()
def init_db_admin():
    Role = core.sch['role']
    User = core.sch['user']

    staff_role = Role(name='user')
    admin_role = Role(name='admin')

    core.orm.session.add(staff_role)
    core.orm.session.add(admin_role)
    core.orm.session.commit()

    test_user = User(
        last_name='Super',
        first_name='Admin',
        email='admin@example.com',
        password=encrypt_password('admin'),
        roles=[staff_role,admin_role],
        active=True,
        confirmed_at=datetime.now(),
    )

    core.orm.session.add(test_user)
    core.orm.session.commit()

    first_names = [
        'Harry', 'Amelia', 'Oliver', 'Jack', 'Isabella', 'Charlie', 'Sophie', 'Mia',
        'Jacob', 'Thomas', 'Emily', 'Lily', 'Ava', 'Isla', 'Alfie', 'Olivia', 'Jessica',
        'Riley', 'William', 'James', 'Geoffrey', 'Lisa', 'Benjamin', 'Stacey', 'Lucy'
    ]
    last_names = [
        'Brown', 'Smith', 'Patel', 'Jones', 'Williams', 'Johnson', 'Taylor', 'Thomas',
        'Roberts', 'Khan', 'Lewis', 'Jackson', 'Clarke', 'James', 'Phillips', 'Wilson',
        'Ali', 'Mason', 'Mitchell', 'Rose', 'Davis', 'Davies', 'Rodriguez', 'Cox', 'Alexander'
    ]

    for i in range(len(first_names)):
        tmp_mail = first_names[i].lower() + "." + last_names[i].lower() + "@example.com"
        tmp_pass = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(10))

        tmp_user = User(
            first_name=first_names[i],
            last_name=last_names[i],
            email=tmp_mail,
            password=encrypt_password(tmp_pass),
            roles=[staff_role,]
        )
        core.orm.session.add(tmp_user)
        core.orm.session.commit()
